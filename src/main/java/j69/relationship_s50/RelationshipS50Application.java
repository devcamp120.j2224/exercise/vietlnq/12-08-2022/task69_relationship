package j69.relationship_s50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelationshipS50Application {

	public static void main(String[] args) {
		SpringApplication.run(RelationshipS50Application.class, args);
	}

}
